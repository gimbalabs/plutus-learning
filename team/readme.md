# Gimbalabs Plutus Project-Based Learning Team

It's November 2021. How can we continue to iterate on our team in "Month 3"?

## What roles do we need?
- Development (all)
    - How should we think about specialized development roles?
    - How can we account for other tasks (listed below) when we account for development outcomes?
- Written Documentation
    - each task gets in own repository, shared across team; this system is working so far
    - high level overview, as in: https://plutus-pioneer-program.readthedocs.io/en/latest/index.html (Adrian?)
    - meeting notes/minutes?
    - Who:
- Video Documentation
    - Our model is great so far. 
    - Does everyone need/want to make videos?
    - Is this a specialized role?
    - Who: 
- Education - Notes from Angela:
    - We discussed a UX Hackathon idea possibly working with Universities from around the world
    - "Q&A Project" -- Tracking questions from Lars' Q&As from both Iteration 1&2
    - The scope of education for Gimbalabs
    - Working with a Professional User Experience professional to help backend developers process the users experience. Beginning to problem solve /create/combine new and old ideas for user experience
    - Roadmap for Uni Education
    - Think up a call to action for UX designer
- Front End "Experiences" to support onboarding of devs to Plutus
    - Still an aspiration, but I'd like to establish this
    - Mobile: Sam?
    - Web/browser based - anyone interested in dedicating some time here?
- User Experience from a back-end perspective
    - Angela talked to Felix about finding a UX designer who might be able to help think about this
    - Let's keep thinking about this --> then define action steps
    - 

## Responsibilities / Specialization?
- Abdelkrim: Video documentation x Front ends
- Adrian: Written documentation - leading readthedocs.io
- Angela: Education - Q&A project
- Eli: Video and Written docs; "leftovers"
- Ganesh: Written docs; readme files can be enhanced; we should go more in depth into Plutus libraries/fundamentals
    - Idea from Angela: review the PAB
- Matthias: Video docs
- Sam: PLUTUS!; mobile; onboarding IOT data (forward looking)
- James: Education; "leftovers"

## Next Steps:
- In the bios folder, please add to your document. Feel free to share any links to social media or personal sites.
- Please add a headshot or avatar to the folder as well.


