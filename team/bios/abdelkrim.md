Name: Abdelkrim Dib

Bio: Software Engineer (JavaScript/C#) | Gimbalabs PPBL Cohort 3 | Plutus Pioneer Cohort 1 | Blockchain Believer

Links:
- Twitter: https://twitter.com/abdelkrimdev
- LinkedIn: https://linkedin.com/in/abdelkrimdev
- GitHub: http://github.com/abdelkrimdev
- Discord: abdelkrimdev#5093
