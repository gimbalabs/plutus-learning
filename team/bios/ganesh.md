Name: Ganesh

Bio: Software engineer aspiring to be a Web3 developer | Gimbalabs PPBL Cohort 1 | Plutus and Prism pioneer | Follower of Cardano

Links:
- Twitter: https://twitter.com/aganeshn
- LinkedIn: https://www.linkedin.com/in/aganeshn/
- YouTube: https://studio.youtube.com/channel/UCWJ-7efMNt_dfRq3t6nlo6w
- Discord: ganesh#9022
- Github: https://github.com/ganeshnithyanandam
