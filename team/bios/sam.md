Name: Sam Jeffrey M

Bio: Aspiring to be a Plutus Pro | Gimbalabs PPBL Cohort 1 | Fullstack Mobile Dev |

Links:
- Twitter: https://twitter.com/spinozious
- LinkedIn: https://www.linkedin.com/in/sam-jeffrey-m-2091731a7/
- YouTube: https://www.youtube.com/channel/UCr6nnih1UXUfStrjF-jBmig
- Discord: samjefree#6995
- Github: https://github.com/SamJeffrey8
- More: https://samjeffrey.web.app/
