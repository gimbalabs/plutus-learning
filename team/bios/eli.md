Name: Eli Selkin

Bio (1-3 sentences?): Eli is a software engineer and a CTO of Loxe Inc and Upful Inc. He leads teams to make cutting edge technologies. Whether Plutus projects or NLP models he's always trying to drive innovation.

Links:
- Twitter: https://twitter.com/SelkinEli
- LinkedIn: https://www.linkedin.com/in/eliselkin/
- YouTube: https://www.youtube.com/watch?v=t05hJzKqr7o&list=PL-ndY6AmLDn6uX5-CS-xV6nbWvJ0v4bzV
- Discord: eponymousEli#4492
- Other: 

Anything else you'd like to include in your bio?
