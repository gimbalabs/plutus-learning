# What did we build in October, 2021?

## 01: Interacting with NFTs in Wallets
#### Initial Task Description:
### 01a: Check that an NFT exists in a Wallet 
- Carefully defining "Wallet" as a collection of UTXOs (Address / Wallet / transaction / UTXO)
### 01b: Get a list of Native Assets in a given UTXO / Wallet
- first, get a list of all native assets and quantity of each (Value)
- if possible, filter this list to show which ones are NFTs -- or, show a technical workaround, way of thinking conceptually about the concept
#### Video Documentation:
https://www.youtube.com/watch?v=t05hJzKqr7o
#### Public Repositories:
https://github.com/eselkin/wallet-probe

## 02: Implement OAuth with NFTs
#### Initial Task Description:
- Building upon tasks 02 and 03 in September, how can we move a few steps closer to an implementation that people actually use?
- Example: an organization/brand can distribute NFTs that represent the issuer; build the mechanism for checking whether that NFT exists.
#### Video Documentation:
https://www.youtube.com/watch?v=nBpMIH84Ifk
#### Public Repositories:
https://github.com/ganeshnithyanandam/loyalty-tokens

## 03: Applied Escrow contract with all necessary endpoints for interaction
#### Initial Task Description:
- Alice is (for example) a Plutus expert
- Bob needs help with Plutus
- Alice is offering 60 minute blocks of time where she offers 1:1 support on Plutus, at a rate of xx Ada/hour
- Bob books an hour with Alice. When booking time, he places xx Ada in this escrow contract
  - After booking the hour, Bob can see specific availability for Alice and can:
  - book time, or
  - get refunded (if no times are compatible)
- Alice meets with Bob
- ...someone says "that meeting happened"
- Funds are released from escrow to Alice
#### Video Documentation:
Video https://www.youtube.com/watch?v=RoR62b7gAKA&ab_channel=SamJeffreyM
#### Public Repositories:
Public Repo: https://github.com/SamJeffrey8/ecrow-contract
Adding Dispute Resolution: https://github.com/ganeshnithyanandam/applied-escrow

## 04 (if time allows): Add dispute resolution on #03
#### Initial Task Description:
- drafting standards so that we can begin to refine
- mediation vs arbitration
- fact finding / data / 
- "meeting didn't happen" vs. "this meeting happened, but there wasn't enough value, I didn't get my money worth"
- thinking ahead: scaling toward higher-value transactions/interactions; standards for different cases/domains
#### Status Update:
Dispute Resolution - we have the start of a good code-walkthrough from what Ganesh shared last Thursday, but it cuts short where we started troubleshooting the emulation. Do we still want to record some sort of conversation about the state of dispute resolution?
#### Public Repositories:

