# PPBL Notepad
What's worth remembering?

## gitlab repo --> gimbalabs.com
- Moving toward folder for each month and file for each task.
- Next: create individual task pages for easy integration with gimbalabs.com

## We had these goals in October:
- Continue to create video documentation for each task
- Continue to publish public repositories sharing our outcomes
- To the extent possible, we'll build live web demos. When this is not possible, we'll talk about what we need in order to get to that point, which will inform future tasks. 
- Worth covering:
  - PAB vs. referencing compiled scripts
  - Testnet CSK with front-end
- Maybe a little something about favorite misconceptions or common pitfalls? Our goal is practice talking about these new tools so that we can achieve a level of semantic clarity.

## Create an overview guidebook for the journey into being a Plutus developer
- Ganesh started this work here: https://docs.google.com/document/d/1hOQl7Ne1JjFbs3sVGaVmmuU8W7gkHTvkayMJUSH3i9I/edit

## Archive

### Continue to check out Alonzo Purple Exercises
- Can we contribute our own take on some of the solutions to Exercise #5 and beyond?
- Github repo: https://github.com/input-output-hk/Alonzo-testnet

### At least for now, we are setting these tasks aside:

#### 04: Update Plutus Pioneer Vesting exercise 
- How can we help developers to understand how time is handled on the Cardano Protocol?
- What tools can we build to make it easier to work with time constraints in Plutus scripts?
- Plutus is in continuous evolution, and as a result the cardano-api can, from time to time, break backward compatibility 
  because of new findings or updates. In this occasion, `SlotRange` in the `TxInfo` has been replaced by `POSIXTimeRange` 
  [commit](https://github.com/input-output-hk/plutus/commit/fbcb8b787a9bcc7ecccb914f9db05a4ac7efb779#diff-7fb2bda211144200604187079d6c51f5be6dcac75e8a2bc2d9b818758c349c3a).
  As an exercise, update the code of `Week3/Homework1` accordingly. Try to put then the Cardano Developer hat on and try to explain why this was necessary and what complications it raises.

#### 05: Metadata validation proof of concept
- How can we help developers understand the difference between "transaction metadata" and the "datum" in a Plutus script?
- Build a proof concept that illustrates the difference, highlighting the utility and the limitations of each.

### Optional Background: Learn a bit about Servant.API
- Tutorial: https://docs.servant.dev/en/stable/tutorial/index.html#cabal-install
- Docs: https://hackage.haskell.org/package/servant


