# What did we build in September, 2021?

## 00: Start Here - get Plutus Starter up and running
#### Intial Task Description:
- A basic example of how we'll interact with the Plutus Application Backend (PAB) in a web-based dapp
- Official Repo: https://github.com/input-output-hk/plutus-starter
- You can use the Plutus Starter project as a starting point for what you'll see in the following tasks.

## 01a: Interact with a basic contract through an endpoint.
#### Intial Task Description:
- What is the minimal contract we can use to demonstrate interaction with a contract via a local endpoint?
- Task: implement the "Gift" contract from Lecture 2 of Plutus Pioneers, in a way that we can interact with it via the PAB
#### Video Documentation:
https://youtu.be/UbTOZWr1-yM

## 01b: Parameterized "Grab"
#### Intial Task Description:
- Task: create a parameterized version of the Grab endpoint and practice accessing it via `curl`
#### Video Documentation:
https://www.youtube.com/watch?v=yeZE5MAjFTI
#### Public Repository:
https://github.com/eselkin/param-pb-pab

## 02: Deliver an Authentication NFT to a Wallet
#### Intial Task Description:
- Review Lecture 5 and build the simplest possible interaction point for getting something like an auth-nft into a simulated wallet.
- We can implement what we saw in Lecture 5. The goal is for an end user to obtain an authentication token by interacting with a web front-end.
#### Video Documentation:
https://youtu.be/NBf8nezLIaU
#### Public Repositories:
- https://github.com/SamJeffrey8/simple-nft-minter
- https://github.com/SamJeffrey8/simple-nft-minter-site


## 03: Use an Authentication NFT to access Encrypted Information
#### Intial Task Description:
- Use an NFT as an authentication token for some privileged access
#### Video Documentation:
https://youtu.be/v7QZsDbpy5M
#### Public Repository:
https://github.com/ganeshnithyanandam/OAuth-NFT