# Plutus Project-Based Learning at Gimbalabs

An open repository where we can collaboratively learn Plutus and build examples to share across the Cardano community.

## Gimbalabs Intentions
1. Create a space where anyone can develop a deeper understanding and get better at building with Plutus.
2. Build an example of a repository of reusable, open source Plutus components (Apache 2.0 License) that will benefit the Cardano community.
3. Practice working together as Gimbalabs begins to take on commercial opportunities.

## Weekly Public Meetings
- Wednesdays at 3:30pm UTC: PPBL Office Hours
- Thursdays at 2pm UTC: PPBL Live Coding
- Details on [Gimbalabs Discord](https://discord.gg/XTvJBj7kzq)

## Plutus Learning Tasks
Each month, the PPBL team takes on a series of tasks. We assume that anyone participating has completed the most recent iteration of [Plutus Pioneers](https://github.com/input-output-hk/plutus-pioneer-program), and seek to start integrating Cardano's smart-contract functionality into a full development stack.

## Browse by Month:
- [September 2021](https://gitlab.com/gimbalabs/plutus-learning/-/tree/main/2021-09-september)
- [October 2021](https://gitlab.com/gimbalabs/plutus-learning/-/tree/main/2021-10-october)
- [November 2021](https://gitlab.com/gimbalabs/plutus-learning/-/tree/main/2021-11-november)

## Each month, the Plutus Project Based Learning (Plutus PBL) team collaborates to:
- complete the monthly list of PBL tasks
- write the next month's list of tasks
- provide documentation in the form of:
    1. written documentation
    2. video walkthroughs
    3. interactive front ends where anyone can explore proofs of concept
- Meet for weekly private retros, public office hours, and public live-coding sessions

## Open Questions

### How will we handle fundamentals?
- How can we create documentation for newcomers so that they can understand eUTXO model, ie:
    - How can I visualize the ledger?
    - How does interaction with Cardano work (ie what endpoints are available?)
    - In general: provide the holistic picture / architecture...
- How can we support new developers to build set up their Plutus development environment?

### How can we collaborate with partners across the Cardano ecosystem?
Longer term, we are building a model for additional start-ups to sponsor additional Plutus PBL devs. This group serves as an initial example for a model that, we expect, will scale.