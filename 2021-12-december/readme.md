# Plutus Project-Based Learning, December 2021
Updated 2021-12-09

## December
This month, for the first time, we're focused on one project: delivering the Unsig NFT Marketplace.

## Unsigs Resources
- [Miro Board](https://miro.com/app/board/uXjVOeT1CLw=/)
- [Front End Repo](https://gitlab.com/gimbalabs/unsigs-frontend)
- [Back End Repo]()
- [Unsigs Site](https://www.unsigs.com/)
- [Spacebudz](https://spacebudz.io/) | [repo](https://github.com/Berry-Pool/spacebudz)

## We are also starting a documentation project
- Outcome: 10 week journey with focused outcomes
- Best Outcome: we'll continue iterating on the documentation 
- Make it easier for a newcomer to understand what is going on
- Balance use of [GitLab](https://gitlab.com/gimbalabs/plutus-learning), [ReadTheDocs](https://ppbl-docs.readthedocs.io/en/latest/) site and [gimbalabs.com](https://gimbalabs.com/pbl/plutus). LMS is an option but only if needed.