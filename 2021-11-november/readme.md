# Plutus Project-Based Learning, November 2021
Updated 2021-11-03

## November
As PPBL grows and evolves, so does the format of our task list. This month, we will take some steps toward specializing team roles and categorizing outcomes. We also focus on one "big" deliverable, the NFT Marketplace.

### Our team is growing!

At a high level, our goals are to deliver an NFT marketplace and to continue with Escrow/Disupte Resolution. Beyond that, we want to further establish systems/roles within our growing PPBL team. [Take a look at the new team page](https://gitlab.com/gimbalabs/plutus-learning/-/tree/main/team) for a place reach consensus on how we'll think about roles.

### November Primary Goals
1. Deliver NFT Marketplace for Unsigs
2. Continue building dispute resolution into escrow contracts built in October
2. Specialize roles with an eye toward scaling PPBL
3. Further elucidate education outcomes
4. Further develop user-friendly front-ends that sufficiently illustrate the use of what we built in September and October, especially with regards to PAB rollout: what can we do now without PAB? what can we do to prepare for PAB?

### Project #1: Deliver NFT Marketplace for Unsigs (and beyond)
- Start by forking https://github.com/Berry-Pool/spacebudz

#### Sub-Components
- Contract address that holds NFT
- Make it easy to change parameters for Royalties + treasury funding
- Front end: build on what's in the original spacebudz repo
    - Mobile: Gatsby; Flutter/Typescript stack?

### Project #2: Escrow and Dispute Resolution
- Minimum outcome: Let's build rails for mediators to resolve disputes when two parties can't agree.
    - State machine solution?
    - Do we need a way to do this without state machines?
- Who pays when there's a dispute?
- Explore: How can we use code to encourage users to resolve disputes amongst themselves (i.e. without mediation)?

### Operationalizing PPBL: Specializing roles
- Driving Plutus Development: Whole team
- Documentation: Any need to specialize?
- Front-end development of:
    a. Public/user-facing examples
    b. In deliverables, as with NFT Marketplace
- Education: Angela and James 
- "UX from a back-end perspective": Angela and James

### Operationalizing PPBL: Education + Documentation
- Angela is piloting a process for cataloging unresolved questions from Plutus Pioneers Office Hours and will share a workflow.
    - Miro board
    - Video draft
    - James supporting
- What role can GameChanger Wallet + Adriano play here?
- Documentation: [Read The Docs site by Adrian](https://ppbl-docs.readthedocs.io/en/latest/)
- [Cardano DApp Development playlist from Abdelkrim](https://www.youtube.com/playlist?list=PL3Agwq8cTKxVefs1QIcfkKGz-_JY-UkIJ)

### Operationalizing PPBL: Building upon prior Outcomes
- How can we use front-end experiences to expose more people to what Plutus is and how it works?
- Deepening outcomes from September and October
- What can we do with authentication?
- What is the utility of Prior Outcomes in connection to NFT Marketplace? in dispute resolution/reputation?

### Additional Ideas
- Token Faucet (see Matthias)
- Continue to build upon the examples Ganesh has shared: bash "walkthroughs" and/or contract emulation/testing.




# Notes from gdoc
## Confident:
1. Serialization libraries
    - Creating Datum
    - Creating Redeemers
    - Configuring each
    - All in all, creating parameters for script
2. Setting up Validator
    - Compiling
    - Using compiled Plutus script byte-code in front end
3. Calling endpoint from script
4. Building and signing transactions...
    - ...that reference the compiled script
    - ...and that include the hashed datum
5. Consuming and using tx metadata in “off-chain” code (not really Plutus, but this is an essential tool that devs will use)

## Current Questions:
1. Coin selection - can we make it less of a ‘black box’?
    - UTxO consolidation component (wallet)
    - User message: “do you want to consolidate your utxos?”
2. How to handle multi-sig transactions in dapp environment. 
    - Specifically, for this use case, we want a marketplace with multiple owners.
    - We also want to extend beyond: for example, what if two (or more) parties want to pool their funds to purchase an NFT?
    - WHO signs WHAT and WHEN? (connection to escrow contract; dispute resolution)
        - Many scenarios: We can address a little at a time, and keep building in the background
3. Wallet Interaction
    - We could build a marketplace that requires use of a particular wallet (Nami, etc…), we’d rather build a marketplace without that restriction -- if possible!
    - Do we require the PAB for this?
    - Or, can we build “a” PAB to handle what we need? (Community, cutting edge)
    - (SpaceBudz requires Nami)
4. LanguageViews - why do we need this when creating transactions?
5. What are the alternatives to dedicated Bid tokens? (If any?)
    - Offchain DB?
    - Side chain -- Logosphere or Fluree?
6. Experimenting with more complex datum types
    - `TransactionLimitException` @ 16kb
    - If we implement a complex Validator, this doesn’t leave much room for anything else
        - For example, if you need to use multiple UTxO’s as input, that creates a data payload…
        - We need to account for real users with “messy” wallets full of little UTxO’s → how does this relate to wallet integration questions?

## Next Steps:
1. Abdelkrim
    - (Keep identifying questions!)
    - Optimize existing code base, prepare for sharing
    - Experiment with other Redeemer options + more complex datum types
2. Abdelkrim + whole team: What APIs are we consuming? What endpoints do we rely upon?
    - Let’s try to use Dandelion, and iterate with Roberto
    - Make a list of questions for Roberto
    - https://gimbalabs.com/dandelion/postgrest-api
    - https://gimbalabs.com/dandelion/graphql-api
3. Team:
    - Work with options for signing and submitting transactions in a way that (eventually) removes dependency on any given wallet → this is what we mean by “building a PAB”; and/or including wallet functionality in a marketplace (or dapp in general)
    - Documentation -- on developer end: let’s distinguish between “redeeming” a transaction vs. “signing” the transaction
    - Commission, royalties, treasury funding - parameters inside of marketplace contract
        - Commission goes to mktplace operator
        - Royalties to creators
        - Treasury funding to related DAO(s)

